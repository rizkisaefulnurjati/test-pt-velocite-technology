<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;
    protected $table = 'master_barang';

    protected $fillable = [
        "nama_barang",
        "harga_satuan"
    ];

    // public function jumlah(){
    //     return $this->belongsTo(Jumlah::class,'master_barang_id','id');
    // }

    // public function transaksi(){
    //     return $this->belongsTo(Transaksi::class,'master_barang_id','id');
    // }


}
    