<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jumlah extends Model
{
    use HasFactory;
    protected $table = 'transaksi_pembelian_barang';

    protected $fillable = [
        "master_barang_id",
        "transaksi_pembelian_id",
        "total_harga"
    ];

    public function transaksi(){
        return $this->hasMany(Transaksi::class,'transaksi_pembelian_id','id');
    }

}
