<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $table = 'transaksi_pembelian';

    protected $fillable = [
        "master_barang_id",
        "total_harga"
    ];

    public function barangs(){
        return $this->belongsTo(Barang::class,'master_barang_id','id');
    }

    public function jumlah(){
        return $this->belongsTo(Jumlah::class,'transaksi_pembelian_id','id');
    }

}
