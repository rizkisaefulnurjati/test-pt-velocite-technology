<?php

namespace Database\Seeders;

use App\Models\Barang;
use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $barang = new Barang();
        $barang->nama_barang = "Sabun batang";
        $barang->harga_satuan = 3000;
        $barang->save();

        $barang = new Barang();
        $barang->nama_barang = "Mi Instan";
        $barang->harga_satuan = 2000;
        $barang->save();
        
        $barang = new Barang();
        $barang->nama_barang = "Pensil";
        $barang->harga_satuan = 1000;
        $barang->save();

        $barang = new Barang();
        $barang->nama_barang = "Kopi Sachet";
        $barang->harga_satuan = 1500;
        $barang->save();

        $barang = new Barang();
        $barang->nama_barang = "Air minum galon";
        $barang->harga_satuan = 20000;
        $barang->save();
                   
    }
}
