@extends('layouts.layouts')

@section('content')

    <!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">List Barang</h1>
    </div>
</div>
<!-- /.container-fluid -->
<div class="container">
  <form action="{{route('transaksi.store')}}" method="POST">
@csrf
      <div class="form-group">
        <label for="exampleFormControlInput1">Qty</label>
        <input type="number" id="qty">
      </div>
      <div class="form-group">
        <label for="exampleFormControlSelect1">Pilih Barang</label>       
        <select class="form-control" id="master_barang_id" name="master_barang_id" onchange="total()">
        @foreach ($barang as $b) 
        <option value={{ $b->id }}><span>{{ $b->nama_barang }}</span> harga: <span id="harga">{{$b->harga_satuan }}</span></option>
        @endforeach
        </select>        
      </div>

      <div class="form-group">
        <label for="exampleFormControlInput1">Total Harga</label>
        <input type="number" value="" id="total_harga" name="total_harga">
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-success">Beli</button>
      </div>


</form>
</div>
<script>
  function total() {
    var jumlah = document.getElementById("harga").value;
    var kuantiti = documen.getElementById("qty").value;
    var hasil = parseInt(jumlah)+parseInt(kuantiti);
    document.getElementById("total_harga").value = hasil;
}
</script>

@endsection