@extends('layouts.layouts')

@section('content')

    <!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">List Barang</h1>
    </div>
</div>
<!-- /.container-fluid -->
<div class="container">
<form action="{{route('barang.store')}}" method="POST">
    @csrf
  <div class="form-group">
    <label for="nama_barang">Nama Barang</label>
    <input type="text" class="form-control" id="nama_barang" name="nama_barang" >
  </div>
  <div class="form-group">
    <label for="nama_barang">Harga Satuan</label>
    <input type="text" class="form-control" id="nama_barang" name="harga_satuan">
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-success">Create</button>
  </div>
</form>
</div>

@endsection