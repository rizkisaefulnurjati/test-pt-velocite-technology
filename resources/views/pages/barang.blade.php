@extends('layouts.layouts')

@section('content')

    <!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">List Barang</h1>
    </div>
</div>
<!-- /.container-fluid -->
<div class="container">
    <div class="row">
        <a href="{{ url('barang/create') }}"><button class="btn btn-success">Tambah barang</button></a>
    </div>
    <br>
    <table class="table table-dark">
        <thead>
          <tr>
            <th scope="col">NO</th>
            <th scope="col">Nama Barang</th>
            <th scope="col">Harga Satuan</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @php
                $no=1;
            @endphp
            @foreach ($barang as $b)  
          <tr>            
            <th scope="row">{{ $no++ }}</th>
            <td>{{ $b->nama_barang }}</td>
            <td>{{ $b->harga_satuan }}</td>
            <td>
                <form 
                    action = "{{ route('barang.destroy', $b->id)}}" method="POST">
                    <a href="{{route('barang.edit', $b->id)}}" class="btn btn-warning">Edit</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>

          </tr>
          @endforeach
        </tbody>
      </table>

</div>

@endsection