    <!-- Bootstrap core JavaScript-->
    <script src="{{ url('bootstrap/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ url('bootstrap/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ url('bootstrap/js/sb-admin-2.min.js') }}"></script>

    <!-- Page level plugins -->
    <script src="{{ url('bootstrap/vendor/chart.js/Chart.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ url('bootstrap/js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ url('bootstrap/js/demo/chart-pie-demo.js') }}"></script>